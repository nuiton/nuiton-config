package org.nuiton.config.io.ini;

/*-
 * #%L
 * Nuiton Config :: IO Ini
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.config.io.ApplicationConfigIO;
import org.nuiton.config.io.ApplicationConfigIOHelper;
import org.nuiton.config.io.ApplicationConfigReadFormat;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by tchemit on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ApplicationConfigIOIniTest {

    @Test
    public void readProperties() throws Exception {

        ApplicationConfigIO io = new ApplicationConfigIOIni();

        File file = Paths.get(new File("").getAbsolutePath(), "src", "test", "resources", "NuitonConfigExample.ini").toFile();

        Properties properties = io.readProperties(file.toURI().toURL(), StandardCharsets.UTF_8.name());

        assertProperties(properties);

    }

    @Test
    public void readPropertiesFromHelper() throws Exception {

        File file = Paths.get(new File("").getAbsolutePath(), "src", "test", "resources", "NuitonConfigExample.ini").toFile();

        Properties properties = new ApplicationConfigIOHelper(ApplicationConfigReadFormat.ini).readProperties(file.toURI().toURL(), StandardCharsets.UTF_8.name());

        assertProperties(properties);

    }

    @Test
    public void writeProperties() throws Exception {

        ApplicationConfigIO io = new ApplicationConfigIOIni();

        Properties properties = new Properties();
        properties.put("identity.firstName", "Joshua");
        properties.put("identity.lastName", "Bloch");
        properties.put("identity.age", "56");
        properties.put("identity.twitter", "jbloch");

        File file = Paths.get(new File("").getAbsolutePath(), "target", "surefire-workdir", "NuitonConfigExample" + System.currentTimeMillis() + ".ini").toFile();

        io.writeProperties(properties, file, StandardCharsets.UTF_8.name(), null);

        Properties properties2 = io.readProperties(file.toURI().toURL(), StandardCharsets.UTF_8.name());

        assertProperties(properties2);

    }

    @Test
    public void writePropertiesFromHelper() throws Exception {

        Properties properties = new Properties();
        properties.put("identity.firstName", "Joshua");
        properties.put("identity.lastName", "Bloch");
        properties.put("identity.age", "56");
        properties.put("identity.twitter", "jbloch");

        File file = Paths.get(new File("").getAbsolutePath(), "target", "surefire-workdir", "NuitonConfigExample" + System.currentTimeMillis() + ".ini").toFile();
        new ApplicationConfigIOHelper(ApplicationConfigReadFormat.ini).writeProperties(properties, file, StandardCharsets.UTF_8.name(), null);

        Properties properties2 = new ApplicationConfigIOHelper(ApplicationConfigReadFormat.ini).readProperties(file.toURI().toURL(), StandardCharsets.UTF_8.name());

        assertProperties(properties2);

    }

    private void assertProperties(Properties properties) {
        Assert.assertNotNull(properties);
        Assert.assertNotNull(properties.getProperty("identity.firstName"));
        Assert.assertEquals("Joshua", properties.getProperty("identity.firstName"));
        Assert.assertEquals("Bloch", properties.getProperty("identity.lastName"));
        Assert.assertEquals("56", properties.getProperty("identity.age"));
        Assert.assertEquals("jbloch", properties.getProperty("identity.twitter"));
    }

}