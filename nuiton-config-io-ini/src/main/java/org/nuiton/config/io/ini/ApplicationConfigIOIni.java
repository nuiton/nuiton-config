package org.nuiton.config.io.ini;

/*-
 * #%L
 * Nuiton Config :: IO Ini
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.nuiton.config.io.ApplicationConfigIO;
import org.nuiton.config.io.ApplicationConfigReadFormat;
import org.nuiton.config.io.ApplicationConfigReadPropertiesException;
import org.nuiton.config.io.ApplicationConfigWritePropertiesException;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * Created on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
public class ApplicationConfigIOIni implements ApplicationConfigIO {

    @Override
    public boolean accept(ApplicationConfigReadFormat readFormat) {
        return ApplicationConfigReadFormat.ini.equals(readFormat);
    }

    @Override
    public Properties readProperties(URL url, String encoding) throws ApplicationConfigReadPropertiesException {
        Properties properties = new Properties();
        INIConfiguration iniConfiguration = new INIConfiguration();
        try (Reader reader = new BufferedReader(new InputStreamReader(url.openStream(), encoding))) {
            iniConfiguration.read(reader);
        } catch (Exception e) {
            throw new ApplicationConfigReadPropertiesException("Can't read ini file: " + url, e);
        }

        Iterator<String> keysIterator = iniConfiguration.getKeys();
        while (keysIterator.hasNext()) {
            String key = keysIterator.next();
            properties.put(key, iniConfiguration.getProperty(key));
        }
        for (String sectionName : iniConfiguration.getSections()) {

            if (sectionName == null) {
                continue;
            }

            SubnodeConfiguration section = iniConfiguration.getSection(sectionName);

            Iterator<String> sectionKeysIterator = section.getKeys();
            while (sectionKeysIterator.hasNext()) {
                String key = sectionKeysIterator.next();
                properties.put(sectionName + "." + key, section.getProperty(key));
            }

        }

        return properties;
    }

    @Override
    public void writeProperties(Properties properties, File file, String encoding, String comment) throws ApplicationConfigWritePropertiesException {

        INIConfiguration iniConfiguration = new INIConfiguration();

        Multimap<String, String> splits = ArrayListMultimap.create();

        for (String key : properties.stringPropertyNames()) {
            List<String> split = new ArrayList<>(Arrays.asList(key.split("\\.")));
            if (split.size() == 1) {
                // simple property
                iniConfiguration.addProperty(key, properties.getProperty(key));
            } else {
                String lastKey = split.remove(split.size() - 1);
                splits.put(Joiner.on(".").join(split), lastKey);
            }
        }

        for (String sectionName : splits.keySet()) {
            Collection<String> keyNames = splits.get(sectionName);
            if (keyNames.size() > 1) {

                // use section
                SubnodeConfiguration section = iniConfiguration.getSection(sectionName);
                for (String keyName : keyNames) {
                    section.addProperty(keyName, properties.getProperty(sectionName + "." + keyName));
                }

            } else {

                // simple property
                String keyName = keyNames.iterator().next();
                iniConfiguration.addProperty(sectionName + "." + keyName, properties.getProperty(sectionName + "." + keyName));
            }
        }

        try (Writer writer = Files.newWriter(file, Charset.forName(encoding))) {
            iniConfiguration.write(writer);
        } catch (Exception e) {
            throw new ApplicationConfigWritePropertiesException("Can't write ini file: " + file, e);
        }

    }

}
