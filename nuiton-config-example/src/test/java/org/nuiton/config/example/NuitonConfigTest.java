package org.nuiton.config.example;

/*-
 * #%L
 * Nuiton Config :: Example
 * %%
 * Copyright (C) 2016 - 2020 Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.config.io.ApplicationConfigReadFormat;

public class NuitonConfigTest {

    @Test
    public void testEnum() {
        // In this test case, we will check that plugin generation will handle correctly enum values
        Assert.assertEquals("ini", NuitonConfigExampleConfigOption.PREFERRED_FORMAT.getDefaultValue());

        NuitonConfigExampleConfig config = new NuitonConfigExampleConfig();
        ApplicationConfigReadFormat preferredFormat = config.getPreferredFormat();
        Assert.assertEquals(ApplicationConfigReadFormat.ini, preferredFormat);
    }

}
