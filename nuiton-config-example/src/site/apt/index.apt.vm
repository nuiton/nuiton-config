~~~
~~ #%L
~~ Nuiton Config :: Example
~~ %%
~~ Copyright (C) 2016 Code Lutin, Tony Chemit
~~ %%
~~ This program is free software: you can redistribute it and/or modify
~~ it under the terms of the GNU Lesser General Public License as
~~ published by the Free Software Foundation, either version 3 of the
~~ License, or (at your option) any later version.
~~ 
~~ This program is distributed in the hope that it will be useful,
~~ but WITHOUT ANY WARRANTY; without even the implied warranty of
~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
~~ GNU General Lesser Public License for more details.
~~ 
~~ You should have received a copy of the GNU General Lesser Public
~~ License along with this program.  If not, see
~~ <http://www.gnu.org/licenses/lgpl-3.0.html>.
~~ #L%
~~~
                                ----
                        Nuiton Config Exemple
                                ----
                                ----
                             2016-09-30
                                ----

Utilisation

  Ce module est un exemple d'utilisation de <<nuiton-config>>.

  Le principe est simple :

  [[1]] Décrire la configuration dans un fichier texte ;

  [[2]] Générer via le plugin maven le code java à partir de la description ;

  [[3]] Utiliser la classe java de configuration qui a été générée ;

  [[4]] Documenter votre configuration via le plugin de report.

  []

  Vous pouvez télécharger les sources du projet {{{https://nexus.nuiton.org/nexus/service/local/artifact/maven/redirect?r=public&g=org.nuiton&a=nuiton-config-example&v=${project.version}&e=zip&c=full}ici}}.

  Dans cet exemple, nous utilisations toutes les valeurs par défaut fournies par le plugin (ZéroConf).

  Voici le GAV du projet

+------------------------------------------------
  <groupId>${project.groupId}</groupId>
  <artifactId>${project.artifactId}</artifactId>
  <version>${project.version}</version>
+------------------------------------------------

  En utilisant les conventions du plugin, la classe de configuration se nommera <<org.nuiton.config.example.NuitonConfigExampleConfig>>.

* Décrire les options et les actions de la configuration

  Dans notre exemple, nous avons quatre options dans la configuration :

  [[1]] <<lastName>> le nom de l'utilisateur

  [[2]] <<firstName>> le prénom de passe de l'utilisateur

  [[3]] <<email>> le courriel de l'utilisateur

  [[4]] <<twitter>> le compte twitter de l'utilisateur

  [[5]] <<age>> l'age de l'utilisateur

  []

  Nous avons aussi défini une action :

  * <<help>> pour afficher l'aide

  []

  Voir le fichier de configuration au format {{{./model/NuitonConfigExample.ini}Ini}},
  {{{./model/NuitonConfigExample.toml}Toml}} ou {{{./model/NuitonConfigExample.yaml}Yaml}}.

* Générer les classes de configuration

  Utiliser le mojo <<generate>> pour générer les classes.

+------------------------------------------------
      <plugin>
        <groupId>${project.groupId}</groupId>
        <artifactId>nuiton-config-maven-plugin</artifactId>
        <version>${project.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>generate</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
+------------------------------------------------

  Cela va générer les classes suivantes :

+------------------------------------------------
target
└── generated-sources
    └── java
        ├── META-INF
        │   └── services
        │       └── org.nuiton.config.ApplicationConfigProvider             (1)
        └── org
            └── nuiton
                └── config
                    └── example
                        ├── GeneratedNuitonConfigExampleConfig.java         (2)
                        ├── GeneratedNuitonConfigExampleConfigProvider.java (3)
--------------------->  ├── NuitonConfigExampleConfig.java                  (4) <----------------------------
                        ├── NuitonConfigExampleConfigAction.java            (5)
                        ├── NuitonConfigExampleConfigOption.java            (6)
                        └── NuitonConfigExampleConfigProvider.java          (7)

+------------------------------------------------

  [[1]] fichier qui enregistre (en utilisant le mécanisme ServiceLoader), le provider de configuration généré

  [[2]] classe abstraite de configuration

  [[3]] classe abstraite de provider

  [[4]] <<classe de configuration prête à l'emploi dans votre application>>

  [[5]] classe qui décrit les actions

  [[6]] classe qui décrit les options

  [[7]] provider de configuration prêt à l'emploi (il sert pour la génération de la documentation notamment)

  []

  <« Et Voila !»>, vous pouvez utiliser la classe de configuration qui est prête à l'emploi.

  Retrouver le code source de toues les classes du projet ({{{./xref/index.html}ici}}).

* Générer la documentation de la configuration

  Utiliser le mojo de report <<report>> ou <<aggregate-report>> pour générer la documentation sur les configurations
  détectées via les providers.

+------------------------------------------------
      <plugin>
        <groupId>${project.groupId}</groupId>
        <artifactId>nuiton-config-maven-plugin</artifactId>
        <version>${project.version}</version>
        <reportSets>
          <reportSet>
            <reports>
              <report>report</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
+------------------------------------------------

  ({{{./config-report.html}Voir rapport généré}}).
