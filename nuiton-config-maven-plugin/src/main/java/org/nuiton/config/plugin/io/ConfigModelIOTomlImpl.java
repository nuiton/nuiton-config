package org.nuiton.config.plugin.io;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import com.moandjiezana.toml.Toml;
import com.moandjiezana.toml.TomlWriter;
import org.nuiton.config.plugin.model.ConfigModel;
import org.nuiton.config.plugin.model.OptionModel;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Implementation using {@code toml} format.
 *
 * Created on 02/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @plexus.component role="org.nuiton.config.plugin.io.ConfigModelIO" role-hint="toml"
 * @since 3.0
 */
public class ConfigModelIOTomlImpl implements ConfigModelIO {

    @Override
    public ConfigModel read(Path path) throws ReadConfigModelException {

        try (Reader reader = Files.newReader(path.toFile(), StandardCharsets.UTF_8)) {
            Toml toml = new Toml().read(reader);
            ConfigModel configModel = toml.to(ConfigModel.class);

            // re-set the type to get real java class and not your alias
            for (OptionModel optionModel : configModel.getOptions()) {
                optionModel.setType(optionModel.getType());
            }
            return configModel;
        } catch (Exception e) {
            throw new ReadConfigModelException("Can't real toml config model from file: " + path, e);
        }

    }

    @Override
    public void write(ConfigModel configModel, Path path) throws WriteConfigModelException {

        try (Writer writer = Files.newWriter(path.toFile(), StandardCharsets.UTF_8)) {

            new TomlWriter().write(configModel, writer);

        } catch (Exception e) {
            throw new WriteConfigModelException("Can't write toml config model from file: " + path, e);
        }

    }

}
