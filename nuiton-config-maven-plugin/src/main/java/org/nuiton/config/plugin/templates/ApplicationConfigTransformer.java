package org.nuiton.config.plugin.templates;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigInit;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.config.plugin.model.ActionModel;
import org.nuiton.config.plugin.model.ConfigModel;
import org.nuiton.config.plugin.model.OptionModel;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.i18n.I18n;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * To generate configuration java files from the options enum file.
 * <p>
 * Created on 15/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ApplicationConfigTransformer extends ObjectModelTransformerToJava {

    private static final Log log = LogFactory.getLog(ApplicationConfigTransformer.class);

    public static final String PROP_CONFIG = "config";

    private ApplicationConfigTransformerConfig config;

    @Override
    public void transformFromModel(ObjectModel model) {
        super.transformFromModel(model);
        config = getConfiguration().getProperty(PROP_CONFIG, ApplicationConfigTransformerConfig.class);

        ConfigModel configModel = config.getConfigModel();

        boolean useNuitonI18n = config.isUseNuitonI18n();

        String packageName = config.getPackageName();
        String configClassName = config.getConfigClassName();
        String configProviderClassName = config.getConfigProviderClassName();
        String optionsClassName = config.getOptionsClassName();
        List<OptionModel> options = configModel.getOptions();

        String actionsClassName = config.getActionsClassName();
        List<ActionModel> actions = configModel.getActions();
        boolean withActions = CollectionUtils.isNotEmpty(actions);

        String abstractClassName = "Generated" + configClassName;

        generateConfigAbstractClass(packageName, abstractClassName, optionsClassName, actionsClassName, options, withActions);

        if (canGenerate(packageName + "." + configClassName)) {

            generateConfigClass(packageName, configClassName, abstractClassName);

        } else {

            if (log.isDebugEnabled()) {
                log.debug("Skip generation for " + configClassName);
            }

        }

        if (config.isGenerateProvider()) {

            String abstractProviderClassName = "Generated" + configProviderClassName;
            generateProviderAbstractClass(packageName, abstractProviderClassName, configClassName, optionsClassName, actionsClassName, useNuitonI18n, configModel.getDescription(), withActions);

            if (canGenerate(packageName + "." + configProviderClassName)) {

                generateProviderClass(packageName, abstractProviderClassName, configProviderClassName);

            }

        }

        if (canGenerate(packageName + "." + optionsClassName)) {
            generateOptionsClass(packageName, useNuitonI18n, optionsClassName, options);
        }

        if (withActions && canGenerate(packageName + "." + actionsClassName)) {
            generateActionsClass(packageName, actionsClassName, actions, useNuitonI18n);
        }


    }

    private void generateOptionsClass(String packageName, boolean useNuitonI18n, String optionsClassName, List<OptionModel> options) {

        ObjectModelEnumeration output = createEnumeration(optionsClassName, packageName);
        addInterface(output, ConfigOptionDef.class);
        if (useNuitonI18n) {
            addImport(output, I18n.class);
        }

        if (log.isInfoEnabled()) {
            log.info("Generate " + output.getQualifiedName());
        }

        for (OptionModel option : options) {

            String literalName = GeneratorUtil.convertVariableNameToConstantName(option.getName());

            String defaultValue = option.getDefaultValue();
            if (defaultValue != null) {
                defaultValue = "\"" + defaultValue + "\"";
            }
            String description = "\"" + option.getDescription() + "\"";
            if (useNuitonI18n) {
                description = "I18n.n(" + description + ")";
            }
            String literal = ""
                    /*{
    <%=literalName%>(
        <%=option.getType()%>.class,
        "<%=option.getKey()%>",
        <%=description%>,
        <%=defaultValue%>,
        <%=option.isTransient()%>,
        <%=option.isFinal()%>)}*/;

            addLiteral(output, literal);

        }

        addAttribute(output, "type", Class.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "key", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "description", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "defaultValue", String.class, null, ObjectModelJavaModifier.PRIVATE);
        addAttribute(output, "_transient", boolean.class, null, ObjectModelJavaModifier.PRIVATE);
        addAttribute(output, "_final", boolean.class, null, ObjectModelJavaModifier.PRIVATE);

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PACKAGE);
        addParameter(constructor, Class.class, "type");
        addParameter(constructor, String.class, "key");
        addParameter(constructor, String.class, "description");
        addParameter(constructor, String.class, "defaultValue");
        addParameter(constructor, boolean.class, "_transient");
        addParameter(constructor, boolean.class, "_final");
        setOperationBody(constructor, ""
       /*{
        this.type = type;
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this._final = _final;
        this._transient = _transient;
    }*/
        );

        ObjectModelOperation getKey = addOperation(
                output, "getKey", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getKey, Override.class);
        setOperationBody(getKey, ""
    /*{
        return key;
    }*/
        );
        ObjectModelOperation getType = addOperation(
                output, "getType", Class.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getType, Override.class);
        setOperationBody(getType, ""
    /*{
        return type;
    }*/
        );
        ObjectModelOperation getDescription = addOperation(
                output, "getDescription", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDescription, Override.class);
        String descriptionValue = "description";
        if (useNuitonI18n) {
            descriptionValue = "I18n.t(" + descriptionValue + ")";
        }
        setOperationBody(getDescription, ""
    /*{
        return <%=descriptionValue%>;
    }*/
        );

        ObjectModelOperation getDefaultValue = addOperation(
                output, "getDefaultValue", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDefaultValue, Override.class);
        setOperationBody(getDefaultValue, ""
    /*{
        return defaultValue;
    }*/
        );

        ObjectModelOperation setDefaultValue = addOperation(
                output, "setDefaultValue", void.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(setDefaultValue, String.class, "defaultValue");
        addAnnotation(output, setDefaultValue, Override.class);
        setOperationBody(setDefaultValue, ""
    /*{
        this.defaultValue = defaultValue;
    }*/
        );


        ObjectModelOperation isTransient = addOperation(
                output, "isTransient", boolean.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, isTransient, Override.class);
        setOperationBody(isTransient, ""
    /*{
        return _transient;
    }*/
        );

        ObjectModelOperation setTransient = addOperation(
                output, "setTransient", void.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(setTransient, boolean.class, "_transient");
        addAnnotation(output, setTransient, Override.class);
        setOperationBody(setTransient, ""
    /*{
        this._transient = _transient;
    }*/
        );

        ObjectModelOperation isFinal = addOperation(
                output, "isFinal", boolean.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, isFinal, Override.class);
        setOperationBody(isFinal, ""
    /*{
        return _final;
    }*/
        );

        ObjectModelOperation setFinal = addOperation(
                output, "setFinal", void.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(setFinal, boolean.class, "_final");
        addAnnotation(output, setFinal, Override.class);
        setOperationBody(setFinal, ""
    /*{
        this._final = _final;
    }*/
        );

    }

    private void generateActionsClass(String packageName, String actionsClassName, List<ActionModel> actions, boolean useNuitonI18n) {

        ObjectModelEnumeration output = createEnumeration(actionsClassName, packageName);
        addInterface(output, ConfigActionDef.class);

        if (log.isInfoEnabled()) {
            log.info("Generate " + output.getQualifiedName());
        }

        if (useNuitonI18n) {
            addImport(output, I18n.class);
        }

        for (ActionModel action : actions) {

            String literalName = GeneratorUtil.convertVariableNameToConstantName(action.getName());

            String aliases = "";
            if (action.getAliases().length > 0) {
                aliases = ", \"" + Joiner.on("\", \"").join(action.getAliases()) + "\"";
            }
            String description = "\"" + action.getDescription() + "\"";
            if (useNuitonI18n) {
                description = "I18n.n(" + description + ")";
            }
            String literal = ""
                    /*{
    <%=literalName%> ( "<%=action.getAction()%>", <%=description%><%=aliases%>  )}*/;

            addLiteral(output, literal);
        }

        addAttribute(output, "action", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "description", String.class, null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(output, "aliases", "String[]", null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PACKAGE);
        addParameter(constructor, String.class, "action");
        addParameter(constructor, String.class, "description");
        addParameter(constructor, "String...", "aliases");
        setOperationBody(constructor, ""
       /*{
        this.action = action;
        this.description = description;
        this.aliases = aliases;
    }*/
        );

        ObjectModelOperation getAction = addOperation(
                output, "getAction", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getAction, Override.class);
        setOperationBody(getAction, ""
    /*{
        return action;
    }*/
        );
        ObjectModelOperation getAliases = addOperation(
                output, "getAliases", "String[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getAliases, Override.class);
        setOperationBody(getAliases, ""
    /*{
        return aliases;
    }*/
        );

        ObjectModelOperation getDescription = addOperation(
                output, "getDescription", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDescription, Override.class);
        String descriptionValue = "description";
        if (useNuitonI18n) {
            descriptionValue = "I18n.t(" + descriptionValue + ")";
        }
        setOperationBody(getDescription, ""
    /*{
        return <%=descriptionValue%>;
    }*/
        );
    }

    private void generateProviderAbstractClass(String packageName, String providerClassName, String className, String optionClassName, String actionClassName, boolean useNuitonI18n, String description, boolean withActions) {

        ObjectModelClass output = createAbstractClass(providerClassName, packageName);
        addInterface(output, ApplicationConfigProvider.class);

        if (log.isInfoEnabled()) {
            log.info("Generate " + output.getQualifiedName());
        }

        ObjectModelOperation getName = addOperation(
                output, "getName", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getName, Override.class);
        setOperationBody(getName, ""
    /*{
        return "<%=className%>";
    }*/
        );

        if (useNuitonI18n) {
            addImport(output, I18n.class);
        }

        String optionClassSimpleName = GeneratorUtil.getSimpleName(optionClassName);

        ObjectModelOperation getOptions = addOperation(
                output, "getOptions", optionClassName + "[]", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getOptions, Override.class);
        setOperationBody(getOptions, ""
    /*{
        return <%=optionClassSimpleName%>.values();
    }*/
        );

        if (withActions) {

            String actionClassSimpleName = GeneratorUtil.getSimpleName(actionClassName);

            ObjectModelOperation getActions = addOperation(
                    output, "getActions", actionClassName + "[]", ObjectModelJavaModifier.PUBLIC);
            addAnnotation(output, getActions, Override.class);
            setOperationBody(getActions, ""
    /*{
        return <%=actionClassSimpleName%>.values();
    }*/
            );
        } else {

            addImport(output, ConfigActionDef.class);
            ObjectModelOperation getActions = addOperation(
                    output, "getActions", "ConfigActionDef[]", ObjectModelJavaModifier.PUBLIC);
            addAnnotation(output, getActions, Override.class);
            setOperationBody(getActions, ""
    /*{
        return new ConfigActionDef[0];
    }*/
            );
        }

        String descriptionValue = "\"" + description + "\"";
        if (useNuitonI18n) {
            descriptionValue = "I18n.l(locale, " + descriptionValue + ")";
        }
        ObjectModelOperation getDescription = addOperation(
                output, "getDescription", String.class, ObjectModelJavaModifier.PUBLIC);
        addParameter(getDescription, Locale.class, "locale");
        addAnnotation(output, getDescription, Override.class);

        setOperationBody(getDescription, ""
    /*{
        return <%=descriptionValue%>;
    }*/
        );
    }

    private void generateProviderClass(String packageName, String abstractProviderClassName, String providerClassName) {

        ObjectModelClass output = createClass(providerClassName, packageName);
        setSuperClass(output, abstractProviderClassName);
        addImport(output, I18n.class);

        if (log.isInfoEnabled()) {
            log.info("Generate " + output.getQualifiedName());
        }


    }

    private void generateConfigAbstractClass(String packageName, String abstractClassName, String optionClassName, String actionClassName, List<OptionModel> options, boolean withActions) {

        String optionClassSimpleName = GeneratorUtil.getSimpleName(optionClassName);

        ObjectModelClass output = createAbstractClass(abstractClassName, packageName);

        addInterface(output, "java.util.function.Supplier<ApplicationConfig>");
        addImport(output, "java.util.function.Supplier");
        addImport(output, optionClassName);

        if (log.isInfoEnabled()) {
            log.info("Generate " + output.getQualifiedName());
        }
        addAttribute(output, "applicationConfig", ApplicationConfig.class, "", ObjectModelJavaModifier.PRIVATE);

        ObjectModelOperation publicConstructor = addConstructor(output, ObjectModelJavaModifier.PROTECTED);
        setOperationBody(publicConstructor, ""
                       /*{
        this(ApplicationConfigInit.forAllScopes());
    }*/
        );

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PROTECTED);
        addParameter(constructor, ApplicationConfigInit.class, "init");
        StringBuilder builder = new StringBuilder();
        builder.append(""
                       /*{
        this.applicationConfig = new ApplicationConfig(init);
        this.applicationConfig.loadDefaultOptions(<%=optionClassSimpleName%>.values());
    }*/
        );
        if (withActions) {
            addImport(output, actionClassName);
            String actionClassSimpleName = GeneratorUtil.getSimpleName(actionClassName);
            builder.append(""
                       /*{    this.applicationConfig.loadActions(<%=actionClassSimpleName%>.class);
    }*/
            );
        }
        setOperationBody(constructor, builder.toString());

        ObjectModelOperation getApplicationConfig = addOperation(
                output, "get", ApplicationConfig.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getApplicationConfig, Override.class);
        setOperationBody(getApplicationConfig, ""
    /*{
        return applicationConfig;
    }*/
        );

        ObjectModelOperation setOption = addOperation(
                output, "setOption", "void", ObjectModelJavaModifier.PROTECTED);
        addParameter(setOption, String.class, "key");
        addParameter(setOption, Object.class, "attrName");

        setOperationBody(setOption, ""
    /*{
        applicationConfig.setOption(key, String.valueOf(attrName));
    }*/
        );

        boolean generatePropertyChangeSupport = config.isGeneratePropertyChangeSupport();
        for (OptionModel option : options) {

            String attrName = option.getName();
            String attrType = option.getType();
            String simpleType = JavaGeneratorUtil.getSimpleName(attrType);
            String constantName = JavaGeneratorUtil.getSimpleName(optionClassName) + "." + JavaGeneratorUtil.convertVariableNameToConstantName(attrName);

            addImport(output, attrType);

            createGetMethod(output, attrName, simpleType, constantName);
            createSetMethod(output, attrName, simpleType, constantName, generatePropertyChangeSupport);

        }

        ObjectModelOperation getConfigurationDescription = addOperation(
                output, "getConfigurationDescription", String.class, ObjectModelJavaModifier.PUBLIC);
        setOperationBody(getConfigurationDescription, ""
    /*{
        StringBuilder builder = new StringBuilder();
        builder.append("\\n=====================================================================================================================");
        builder.append("\\n=== Configuration ===================================================================================================");
        builder.append("\\n=====================================================================================================================");
        builder.append(String.format("\\n=== %1$-40s = %2$s", "Filename", get().getConfigFileName()));
        for (<%=optionClassSimpleName%> option : orderedByKey()) {
            builder.append(String.format("\\n=== %1$-40s = %2$s", option.getKey(), get().getOption(option.getKey())));
        }
        builder.append("\\n=====================================================================================================================");
        return builder.toString();
    }*/
        );

        if (generatePropertyChangeSupport) {

            addAttribute(output, "pcs", PropertyChangeSupport.class, "new PropertyChangeSupport(this);",
                         ObjectModelJavaModifier.PROTECTED, ObjectModelJavaModifier.FINAL);

            ObjectModelOperation addPropertyChangeListener = addOperation(
                    output, "addPropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
            addParameter(addPropertyChangeListener, PropertyChangeListener.class, "listener");
            setOperationBody(addPropertyChangeListener, ""
     /*{
        pcs.addPropertyChangeListener(listener);
    }*/
            );
            ObjectModelOperation addPropertyChangeListener2 = addOperation(
                    output, "addPropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
            addParameter(addPropertyChangeListener2, String.class, "propertyName");
            addParameter(addPropertyChangeListener2, PropertyChangeListener.class, "listener");
            setOperationBody(addPropertyChangeListener2, ""
     /*{
        pcs.addPropertyChangeListener(propertyName, listener);
    }*/
            );

            ObjectModelOperation removePropertyChangeListener = addOperation(
                    output, "removePropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
            addParameter(removePropertyChangeListener, PropertyChangeListener.class, "listener");
            setOperationBody(removePropertyChangeListener, ""
     /*{
        pcs.removePropertyChangeListener(listener);
    }*/
            );
            ObjectModelOperation removePropertyChangeListener2 = addOperation(
                    output, "removePropertyChangeListener", "void", ObjectModelJavaModifier.PUBLIC);
            addParameter(removePropertyChangeListener2, String.class, "propertyName");
            addParameter(removePropertyChangeListener2, PropertyChangeListener.class, "listener");
            setOperationBody(removePropertyChangeListener2, ""
     /*{
        pcs.removePropertyChangeListener(propertyName, listener);
    }*/
            );

            ObjectModelOperation firePropertyChange = addOperation(
                    output, "firePropertyChange", "void", ObjectModelJavaModifier.PROTECTED);
            addParameter(firePropertyChange, String.class, "propertyName");
            addParameter(firePropertyChange, Object.class, "oldValue");
            addParameter(firePropertyChange, Object.class, "newValue");
            setOperationBody(firePropertyChange, ""
     /*{
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }*/
            );

            ObjectModelOperation firePropertyChange2 = addOperation(
                    output, "firePropertyChange", "void", ObjectModelJavaModifier.PROTECTED);
            addParameter(firePropertyChange2, String.class, "propertyName");
            addParameter(firePropertyChange2, Object.class, "newValue");
            setOperationBody(firePropertyChange2, ""
     /*{
        pcs.firePropertyChange(propertyName, null, newValue);
    }*/
            );
        }

        ObjectModelOperation orderedByKey = addOperation(
                output, "orderedByKey", "List<" + optionClassSimpleName + ">", ObjectModelJavaModifier.PROTECTED);
        addImport(output, List.class);
        addImport(output, Arrays.class);
        addImport(output, Collections.class);
        addImport(output, Comparator.class);
        setOperationBody(orderedByKey, ""
    /*{
        List<<%=optionClassSimpleName%>> values = Arrays.asList(<%=optionClassSimpleName%>.values());
        Collections.sort(values, new Comparator<<%=optionClassSimpleName%>>() {
            @Override
            public int compare(<%=optionClassSimpleName%> o1, <%=optionClassSimpleName%> o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        });
        return Collections.unmodifiableList(values);
    }*/
        );

    }

    private void generateConfigClass(String packageName, String className, String abstractClassName) {

        ObjectModelClass output = createClass(className, packageName);
        if (log.isInfoEnabled()) {
            log.info("Generate " + output.getQualifiedName());
        }
        setSuperClass(output, abstractClassName);

        ObjectModelOperation publicConstructor = addConstructor(output, ObjectModelJavaModifier.PUBLIC);
        setOperationBody(publicConstructor, ""
                       /*{
        super();
    }*/
        );

        ObjectModelOperation constructor = addConstructor(output, ObjectModelJavaModifier.PUBLIC);
        addParameter(constructor, ApplicationConfigInit.class, "init");
        setOperationBody(constructor, ""
                       /*{
        super(init);
    }*/
        );

    }

    private boolean canGenerate(String input) {
        return !getResourcesHelper().isJavaFileInClassPath(input);
    }

    private static final Set<String> KNOWN_TYPES = ImmutableSet.of(
            "File",
            "Color",
            "KeyStroke",
            "URL",
            "Class",
            "Date",
            "Time",
            "Timestamp",
            "Locale",
            "Version",
            "String",
            "int",
            "Integer",
            "long",
            "Long",
            "float",
            "Float",
            "boolean",
            "Boolean",
            "byte",
            "Byte",
            "char",
            "Character",
            "double",
            "Double");

    private void createGetMethod(ObjectModelClass output,
                                 String attrName,
                                 String simpleType,
                                 String constantName) {

        boolean booleanProperty = GeneratorUtil.isBooleanPrimitive(simpleType);
        String methodPrefix = booleanProperty ? JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX : JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;

        String methodName = "getOptionAs" + StringUtils.capitalise(simpleType);
        if (simpleType.equals("String")) {
            methodName = "getOption";
        } else if (simpleType.equals("Integer")) {
            methodName = "getOptionAsInt";
        }

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName(methodPrefix, attrName),
                simpleType,
                ObjectModelJavaModifier.PUBLIC
        );

        if (!KNOWN_TYPES.contains(simpleType)) {
            methodName = "getOptionAsObject";
            setOperationBody(operation, ""
    /*{
        return (<%=simpleType%>) applicationConfig.<%=methodName%>(<%=simpleType%>.class, <%=constantName%>.getKey());
    }*/
            );
        } else

        {
            setOperationBody(operation, ""
    /*{
        return applicationConfig.<%=methodName%>(<%=constantName%>.getKey());
    }*/
            );

            if ("Boolean".equals(simpleType)) {
                operation = addOperation(
                        output,
                        getJavaBeanMethodName(JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX, attrName),
                        "boolean",
                        ObjectModelJavaModifier.PUBLIC
                );

                setOperationBody(operation, ""
    /*{
        return applicationConfig.<%=methodName%>(<%=constantName%>.getKey());
    }*/
                );
            }
        }
    }

    private void createSetMethod(ObjectModelClass output,
                                 String attrName,
                                 String simpleType,
                                 String constantName,
                                 boolean generatePropertyChangeSupport) {
        boolean booleanProperty = GeneratorUtil.isBooleanPrimitive(simpleType);
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("set", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, simpleType, attrName);

        String methodPrefix = booleanProperty ? JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX : JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
        String methodName = getJavaBeanMethodName(methodPrefix, attrName);
        StringBuilder bodyContent = new StringBuilder();
        if (generatePropertyChangeSupport) {
            bodyContent.append(""
    /*{
        Object oldValue = <%=methodName%>();
    }*/

            );
        }
        bodyContent.append(""
    /*{    setOption(<%=constantName%>.getKey(), <%=attrName%>);
    }*/
        );
        if (generatePropertyChangeSupport) {
            bodyContent.append(""
    /*{    firePropertyChange("<%=attrName%>", oldValue, <%=attrName%>);
    }*/
            );
        }

        setOperationBody(operation, bodyContent.toString());

        if ("Boolean".equals(simpleType)) {
            operation = addOperation(
                    output,
                    getJavaBeanMethodName("set", attrName),
                    "void",
                    ObjectModelJavaModifier.PUBLIC
            );
            addParameter(operation, "boolean", attrName);

            setOperationBody(operation, ""
    /*{
        setOption(<%=constantName%>.getKey(), <%=attrName%>);
    }*/
            );
        }


    }

}
