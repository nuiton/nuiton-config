package org.nuiton.config.plugin.model;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ConfigOptionDef;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.util.ObjectUtil;
import org.nuiton.version.Version;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;


/**
 * Created on 01/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class OptionModel {

    private String name;
    private String key;
    private String description;
    private String type;
    private String defaultValue;
    private boolean _transient;
    private boolean _final;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {

        switch (type) {
            case "string":
                this.type = String.class.getName();
                break;
            case "int":
                this.type = int.class.getName();
                break;
            case "Integer":
                this.type = Integer.class.getName();
                break;
            case "float":
                this.type = float.class.getName();
                break;
            case "Float":
                this.type = Float.class.getName();
                break;
            case "long":
                this.type = long.class.getName();
                break;
            case "Long":
                this.type = Long.class.getName();
                break;
            case "double":
                this.type = double.class.getName();
                break;
            case "Double":
                this.type = Double.class.getName();
                break;
            case "boolean":
                this.type = boolean.class.getName();
                break;
            case "Boolean":
                this.type = Boolean.class.getName();
                break;
            case "byte":
                this.type = byte.class.getName();
                break;
            case "Byte":
                this.type = Byte.class.getName();
                break;
            case "char":
                this.type = char.class.getName();
                break;
            case "Character":
                this.type = Character.class.getName();
                break;
            case "file":
                this.type = File.class.getName();
                break;
            case "version":
                this.type = Version.class.getName();
                break;
            case "color":
                this.type = Color.class.getName();
                break;
            case "keystroke":
                this.type = KeyStroke.class.getName();
                break;
            case "url":
                this.type = URL.class.getName();
                break;
            case "date":
                this.type = Date.class.getName();
                break;
            case "time":
                this.type = Time.class.getName();
                break;
            case "timestamp":
                this.type = Timestamp.class.getName();
                break;
            default:
                this.type = type;
                break;
        }
    }

    public void setType(Class type) {


        if (type.equals(String.class)) {
            this.type="string";
        } else if (type.isPrimitive()) {
            this.type=type.getSimpleName();
        } else if (ObjectUtil.isPrimitive(type)) {
            this.type=type.getSimpleName();
        } else if (type.equals(File.class)) {
            this.type="file";
        } else if (type.equals(Version.class)) {
            this.type="version";
        } else if (type.equals(Color.class)) {
            this.type="color";
        } else if (type.equals(KeyStroke.class)) {
            this.type="keystroke";
        } else if (type.equals(URL.class)) {
            this.type="url";
        } else if (type.equals(Date.class)) {
            this.type="date";
        } else if (type.equals(Time.class)) {
            this.type="time";
        } else if (type.equals(Timestamp.class)) {
            this.type="timestamp";
        } else {
            this.type= type.getName();
        }

    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isTransient() {
        return _transient;
    }

    public boolean isFinal() {
        return _final;
    }

    public void setTransient(boolean _transient) {
        this._transient = _transient;
    }

    public void setFinal(boolean _final) {
        this._final = _final;
    }

    public static OptionModel of(ConfigOptionDef configOptionDef) {
        OptionModel optionModel = new OptionModel();

        if (configOptionDef instanceof Enum) {
            Enum optionDef = (Enum) configOptionDef;

            optionModel.setName(GeneratorUtil.convertConstantNameToVariableName(optionDef.name()));

        }
        optionModel.setKey(configOptionDef.getKey());
        optionModel.setType(configOptionDef.getType());
        optionModel.setDefaultValue(configOptionDef.getDefaultValue());
        optionModel.setDescription(configOptionDef.getDescription());
        optionModel.setTransient(configOptionDef.isTransient());
        optionModel.setFinal(configOptionDef.isFinal());
        return optionModel;
    }
}
