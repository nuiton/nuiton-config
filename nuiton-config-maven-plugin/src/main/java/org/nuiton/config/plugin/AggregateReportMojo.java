package org.nuiton.config.plugin;

/*
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.MavenReportException;
import org.nuiton.config.ApplicationConfigProvider;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Generates a report for declared application config via the
 * {@link ApplicationConfigProvider} mechanism on a aggregate mojo.
 * <p>
 * For each configuration, you can find all his options and actions.
 *
 * @author Tony - dev@tchemit.fr
 * @since 3.0
 */
@Mojo(name = "aggregate-report", aggregator = true, requiresReports = true, requiresDependencyResolution = ResolutionScope.RUNTIME)
@Execute(phase = LifecyclePhase.COMPILE)
public class AggregateReportMojo extends ReportMojoSupport {

    /**
     * The projects in the reactor.
     */
    @Parameter(property = "reactorProjects", readonly = true, required = true)
    private List<MavenProject> reactorProjects;

    @Override
    protected ClassLoader createClassLoader() throws MavenReportException {
        Set<String> paths = new HashSet<>();

        for (MavenProject p : reactorProjects) {
            try {
                List<String> runtimeClasspathElements = p.getRuntimeClasspathElements();
                paths.addAll(runtimeClasspathElements);
            } catch (DependencyResolutionRequiredException e) {
                throw new MavenReportException("Could not obtain dependencies for project " + p);
            }
        }
        return createClassLoader(paths);
    }
}
