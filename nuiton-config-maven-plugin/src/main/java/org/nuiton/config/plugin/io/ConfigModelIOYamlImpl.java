package org.nuiton.config.plugin.io;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import org.nuiton.config.plugin.model.ActionModel;
import org.nuiton.config.plugin.model.ConfigModel;
import org.nuiton.config.plugin.model.OptionModel;

import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Implementation using {@code yaml} format.
 *
 * Created on 01/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @plexus.component role="org.nuiton.config.plugin.io.ConfigModelIO" role-hint="yaml"
 * @since 3.0
 */
public class ConfigModelIOYamlImpl implements ConfigModelIO {

    @Override
    public ConfigModel read(Path path) throws ReadConfigModelException {

        try (Reader fileReader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            YamlReader reader = new YamlReader(fileReader, createConfig());
            ConfigModel configModel = reader.read(ConfigModel.class);
            return configModel;
        } catch (Exception e) {
            throw new ReadConfigModelException("Can't real yaml config model from file: " + path, e);
        }

    }


    @Override
    public void write(ConfigModel configModel, Path path) throws WriteConfigModelException {

        try (Writer fileWriter = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
            YamlWriter writer = new YamlWriter(fileWriter, createConfig());
            writer.write(configModel);
            fileWriter.flush();
            writer.close();
        } catch (Exception e) {
            throw new WriteConfigModelException("Can't write yaml config model from file: " + path, e);
        }

    }

    private YamlConfig createConfig() {

        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.setClassTag("option", OptionModel.class);
        yamlConfig.setClassTag("action", ActionModel.class);
        yamlConfig.writeConfig.setIndentSize(2);
        yamlConfig.writeConfig.setWriteRootTags(false);
        return yamlConfig;

    }
}
