package org.nuiton.config.plugin;

/*
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.config.ApplicationConfigHelper;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.config.plugin.model.ActionModel;
import org.nuiton.config.plugin.model.ConfigModel;
import org.nuiton.config.plugin.model.OptionModel;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Describe application config from java files to the description configuration format.
 * <p>
 * Created on 28/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@Mojo(name = "describe", requiresDirectInvocation = true, defaultPhase = LifecyclePhase.COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
@Execute(phase = LifecyclePhase.COMPILE)
public class DescribeMojo extends ConfigMojoSupport {

    /**
     * The name of provider to describe.
     * <p>
     * By default, will use artifactId in camelCase format ( example: provider name for artifact id
     * {@code nuiton-config-example} is {@code NuitonConfigExample} ).
     *
     * @see ApplicationConfigProvider
     */
    @Parameter(property = "providerName")
    private String providerName;

    private ConfigModel configModel;

    @Override
    protected void init() throws Exception {

        super.init();

        if (providerName == null) {

            List<String> artifactIdPaths = new ArrayList<>();
            for (String artifactIdPath : Arrays.asList(getProject().getArtifactId().replaceAll("-", ".").split("\\."))) {
                artifactIdPaths.add(StringUtils.capitalize(artifactIdPath));
            }
            providerName = Joiner.on("").join(artifactIdPaths) + "Config";

        }

        if (isVerbose()) {
            getLog().info("Use provider name: " + providerName);
        }
        ClassLoader loader = initClassLoader(getProject(), new File(getProject().getBuild().getOutputDirectory()), true, false, true, true, true);

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();

        ApplicationConfigProvider provider;
        try {
            Thread.currentThread().setContextClassLoader(loader);
            provider = ApplicationConfigHelper.getProvider(loader, providerName);
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }

        if (provider == null) {
            throw new MojoExecutionException("Could not find provider with name: " + providerName);
        }

        String modelName = StringUtils.removeEnd(provider.getName(), "Config");
        if (isVerbose()) {
            getLog().info("Use model name: " + modelName);
        }
        if (getModelFile() == null) {
            setModelFile(modelName);
        }
        Files.createDirectories(getModelFile().getParentFile().toPath());

        String description = provider.getDescription(Locale.FRANCE);

        configModel = new ConfigModel();
        configModel.setDescription(description);

        LinkedList<OptionModel> optionModels = new LinkedList<>();
        for (ConfigOptionDef configOptionDef : provider.getOptions()) {
            optionModels.add(OptionModel.of(configOptionDef));
        }
        configModel.setOptions(optionModels);

        LinkedList<ActionModel> actionModels = new LinkedList<>();
        for (ConfigActionDef configActionDef : provider.getActions()) {
            actionModels.add(ActionModel.of(configActionDef));
        }
        configModel.setActions(actionModels);

    }

    @Override
    protected void doAction() throws Exception {

        getLog().info("Generate file to: " + getModelFile());

        getIO().write(configModel, getModelFile().toPath());

    }

}
