package org.nuiton.config.plugin;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.config.plugin.io.ConfigModelIO;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;

import java.io.File;
import java.util.Map;

/**
 * Created on 02/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
abstract class ConfigMojoSupport extends AbstractPlugin {

    /**
     * Configuration description format {@code ini}, {@code toml} or {@code yaml}.
     *
     * Default value is {@code ini}.
     */
    @Parameter(property = "config.format", defaultValue = "ini")
    private String format;

    /**
     * The path of model file.
     *
     * <p>Default value is </p>
     * <pre>${modelDirectory}/${modelName}.${format}</pre>
     */
    @Parameter(property = "config.modelFile")
    private File modelFile;

    /**
     * The source directory where to scan model file.
     */
    @Parameter(property = "config.modelDirectory", defaultValue = "${basedir}/src/main/config", required = true)
    private File modelDirectory;

    /**
     * Pour activer le mode verbeux.
     */
    @Parameter(property = "config.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * Maven project.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Component(role = ConfigModelIO.class)
    private Map<String, ConfigModelIO> configModelIs;

    @Override
    protected void init() throws Exception {

        if (!configModelIs.containsKey(format)) {
            throw new MojoExecutionException("Don't know format: " + format + ", use one of these: " + configModelIs.keySet());
        }

        if (isVerbose()) {
            getLog().info("Use format: " + format);
        }

    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    void setModelFile(String name) {
        modelFile = PluginHelper.getFile(modelDirectory, name + "." + format);
    }

    ConfigModelIO getIO() {
        return configModelIs.get(format);
    }

    File getModelFile() {
        return modelFile;
    }
}
