package org.nuiton.config.plugin.io;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.nuiton.config.plugin.model.ActionModel;
import org.nuiton.config.plugin.model.ConfigModel;
import org.nuiton.config.plugin.model.OptionModel;

import java.util.List;

/**
 * Created on 02/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
class ConfigMoldeIOFixtures {

    void assertConfigModel(ConfigModel configModel) {
        Assert.assertNotNull(configModel);
        Assert.assertEquals("Exemple de configuration", configModel.getDescription());

        List<OptionModel> options = configModel.getOptions();
        Assert.assertNotNull(options);
        Assert.assertEquals(5, options.size());
        assertOption(options.get(0), "identity.firstName", "Prénom de l'utilisateur", String.class, "Joshua");
        assertOption(options.get(1), "identity.lastName", "Nom de l'utilisateur", String.class, "Bloch");
        assertOption(options.get(2), "identity.email", "Courriel de l'utilisateur", String.class, null);
        assertOption(options.get(3), "identity.twitter", "Compte Twitter de l'utilisateur", String.class, "jbloch");
        assertOption(options.get(4), "identity.age", "age de l'utilisateur", int.class, "56");

        List<ActionModel> actions = configModel.getActions();
        Assert.assertNotNull(actions);
        Assert.assertEquals(1, actions.size());
        assertAction(actions.get(0), "help", "Pour afficher l'aide", "org.nuiton.config.example.NuitonConfigExample#help", "-h", "--help");
    }

    private void assertOption(OptionModel optionModel, String expectedKey, String expectedDescpription, Class<?> expectedType, String expectedDefaultValue) {
        Assert.assertNotNull(optionModel);
        Assert.assertNotNull(optionModel.getKey());
        Assert.assertEquals(expectedKey, optionModel.getKey());
        Assert.assertNotNull(optionModel.getDescription());
        Assert.assertEquals(expectedDescpription, optionModel.getDescription());
        Assert.assertNotNull(optionModel.getType());
        Assert.assertEquals(expectedType.getName(), optionModel.getType());
        Assert.assertEquals(expectedDefaultValue, optionModel.getDefaultValue());
    }

    private void assertAction(ActionModel actionModel, String expectedName, String expectedDescription, String expectedAction, String... expectedAliases) {
        Assert.assertNotNull(actionModel);
        Assert.assertNotNull(actionModel.getName());
        Assert.assertEquals(expectedName, actionModel.getName());
        Assert.assertNotNull(actionModel.getDescription());
        Assert.assertEquals(expectedDescription, actionModel.getDescription());
        Assert.assertNotNull(actionModel.getAction());
        Assert.assertEquals(expectedAction, actionModel.getAction());
        Assert.assertArrayEquals(expectedAliases, actionModel.getAliases());
    }

}
