package org.nuiton.config.plugin.io;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.config.plugin.model.ConfigModel;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created on 01/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ConfigModelIOYamlImplTest {

    private ConfigModelIO io;
    private ConfigMoldeIOFixtures ioFixtures;

    @Before
    public void setUp() throws Exception {
        io = new ConfigModelIOYamlImpl();
        ioFixtures = new ConfigMoldeIOFixtures();
    }

    @Test
    public void read() throws Exception {

        Path path = Paths.get(new File("").getAbsolutePath(), "src", "test", "resources", "NuitonConfigExample.yaml");

        ConfigModel configModel = io.read(path);

        ioFixtures.assertConfigModel(configModel);

    }

    @Test
    public void write() throws Exception {

        Path path = Paths.get(new File("").getAbsolutePath(), "src", "test", "resources", "NuitonConfigExample.yaml");


        ConfigModel configModel = io.read(path);
        Assert.assertNotNull(configModel);

        Path path2 = Paths.get(new File("").getAbsolutePath(), "target", "surefire-workdir", "NuitonConfigExample2.yaml");
        io.write(configModel, path2);

        ConfigModel configModel2 = io.read(path);
        ioFixtures.assertConfigModel(configModel2);

    }

}
