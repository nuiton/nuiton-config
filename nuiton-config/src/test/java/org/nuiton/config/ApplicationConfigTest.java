package org.nuiton.config;

/*
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;
import org.junit.rules.TestName;
import org.nuiton.util.FileUtil;
import org.nuiton.version.Version;
import org.nuiton.config.ApplicationConfig.Action;
import org.nuiton.version.VersionBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

/**
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.6.10
 */
public class ApplicationConfigTest {

    private static final Log log =
            LogFactory.getLog(ApplicationConfigTest.class);

    public static final long TIMESTAMP = System.nanoTime();

    protected static int DUMMY_ACTION_CALL;

    public static class DummyAction {
        @Action.Step(1)
        public void dummyAction(String s, int step) {
            DUMMY_ACTION_CALL++;
            log.info(s + ':' + step);
        }
    }

    @Rule
    public final TestName testName = new TestName();

    protected File testDirectory;

    @Before
    public void before() {
        testDirectory = FileUtil.getTestSpecificDirectory(
                getClass(),
                testName.getMethodName(),
                null,
                TIMESTAMP);
    }

    @Test
    public void saveForUser() throws IOException {

        // Initialize path and filename
        String path = testDirectory.getAbsolutePath();

        String oldHome = SystemUtils.getUserHome().getAbsolutePath();

        try {
            System.setProperty("user.home", path);

            ApplicationConfig config =
                    new ApplicationConfig(testName.getMethodName());

            File userFile = config.getUserConfigFile();

            if (userFile.exists()) {
                FileUtils.forceDelete(userFile);
            }

            config.setOption("key1", "toto");
            config.setOption("key2", "tata");
            config.setOption("key3", "tutu");

            // Parent directory will be created
            config.saveForUser();

            // I like to test that file.create works :(
            Assert.assertTrue(userFile.exists());

            Properties p = loadPropertyFile(userFile);

            Assert.assertEquals(3, p.size());

            String property;

            property = p.getProperty("key1");
            Assert.assertEquals("toto", property);

            property = p.getProperty("key2");
            Assert.assertEquals("tata", property);

            property = p.getProperty("key3");
            Assert.assertEquals("tutu", property);

        } finally {

            System.setProperty("user.home", oldHome);
        }

    }

    @Test
    public void cleanUserConfig() throws IOException, ArgumentsParserException {

        // Initialize path and filename
        String path = testDirectory.getAbsolutePath();

        String oldHome = SystemUtils.getUserHome().getAbsolutePath();

        try {
            System.setProperty("user.home", path);

            ApplicationConfig config =
                    new ApplicationConfig(testName.getMethodName());


            config.setOption("key1", "toto");
            config.setOption("key2", "");
            config.setOption("key3", "tutu");

            File userFile = config.getUserConfigFile();

            Assert.assertTrue(userFile.getAbsolutePath().startsWith(path));

            if (userFile.exists()) {
                FileUtils.forceDelete(userFile);
            }

            config.saveForUser();

            Assert.assertTrue(userFile.exists());

            Properties p = loadPropertyFile(userFile);

            Assert.assertEquals(3, p.size());

            String property;

            property = p.getProperty("key1");
            Assert.assertEquals("toto", property);

            property = p.getProperty("key2");
            Assert.assertEquals("", property);

            property = p.getProperty("key3");
            Assert.assertEquals("tutu", property);

            // reload config
            config = new ApplicationConfig(testName.getMethodName());
            config.parse();

            config.cleanUserConfig();

            Properties p2 = loadPropertyFile(userFile);

            // key2 was removed
            Assert.assertEquals(2, p2.size());

            property = p2.getProperty("key1");
            Assert.assertEquals("toto", property);

            property = p2.getProperty("key2");
            Assert.assertNull(property);

            property = p2.getProperty("key3");
            Assert.assertEquals("tutu", property);

            // now key3 will be removed (even if not blank)
            config.cleanUserConfig("key3");

            // reload config
            config = new ApplicationConfig(testName.getMethodName());
            config.parse();

            Properties p3 = loadPropertyFile(userFile);

            // key3 was removed
            Assert.assertEquals(1, p3.size());

            property = p3.getProperty("key1");
            Assert.assertEquals("toto", property);
        } finally {

            System.setProperty("user.home", oldHome);
        }

    }

    @Test
    public void getUnparsed() throws Exception {
        ApplicationConfig instance = new ApplicationConfig();
        List<String> expResult = new ArrayList<>();
        List<String> result = instance.getUnparsed();
        Assert.assertEquals(expResult, result);

        expResult.add("toto");
        expResult.add("titi");
        expResult.add("tata");

        instance.parse("toto", "titi", "tata");
        result = instance.getUnparsed();
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void addAction() throws Exception {
        ApplicationConfig instance = new ApplicationConfig();

        // test add null Action
        instance.addAction(null);

        Action action = new Action(1, new DummyAction(), DummyAction.class.getMethod("dummyAction", String.class, Integer.TYPE), "coucou", "12");
        instance.addAction(action);
    }

    @Test
    public void doAction() throws Exception {
        ApplicationConfig instance = new ApplicationConfig();

        Action action = new Action(1, new DummyAction(), DummyAction.class.getMethod("dummyAction", String.class, Integer.TYPE), "coucou", "12");
        instance.addAction(action);

        DUMMY_ACTION_CALL = 0;
        Assert.assertEquals(0, DUMMY_ACTION_CALL);
        instance.doAction(0);
        Assert.assertEquals(0, DUMMY_ACTION_CALL);
        instance.doAction(1);
        Assert.assertEquals(1, DUMMY_ACTION_CALL);
        instance.doAction(2);
        Assert.assertEquals(1, DUMMY_ACTION_CALL);
    }


    @Test
    public void doActionOnApplicationConfig() throws Exception {
        ApplicationConfig instance = new ApplicationConfig();

        instance.parse("--this#toString");

        Map<Integer, List<Action>> actions = instance.actions;

        Assert.assertFalse(actions.isEmpty());
        Assert.assertTrue(actions.containsKey(0));

        instance.doAction(0);
    }

    @Test
    public void setUseOnlyAliases() {
        ApplicationConfig instance = new ApplicationConfig();
        Assert.assertEquals(false, instance.isUseOnlyAliases());
        instance.setUseOnlyAliases(false);
        Assert.assertEquals(false, instance.isUseOnlyAliases());
        instance.setUseOnlyAliases(true);
        Assert.assertEquals(true, instance.isUseOnlyAliases());
    }

    @Test
    public void addAlias() throws Exception {
        ApplicationConfig instance = new ApplicationConfig();
        instance.addAlias("toto", "totochange");
        instance.addAlias("titi", "titichange");

        List<String> expResult = new ArrayList<>();
        List<String> result = instance.getUnparsed();
        Assert.assertEquals(expResult, result);

        expResult.add("totochange");
        expResult.add("titichange");
        expResult.add("tata");

        instance.parse("toto", "titi", "tata");
        result = instance.getUnparsed();
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testSetConfigFileName() {
        ApplicationConfig instance = new ApplicationConfig();
        instance.setConfigFileName("bidulle");
        Assert.assertEquals("bidulle", instance.getConfigFileName());
    }

    /** Test of setOption method, of class ApplicationConfig. */
    @Test
    public void testSetOption() {
        ApplicationConfig instance = new ApplicationConfig();
        Assert.assertEquals(null, instance.getOption("truc"));
        instance.setOption("truc", "bidulle");
        Assert.assertEquals("bidulle", instance.getOption("truc"));
    }

    @Test
    public void testSubConfig() {
        ApplicationConfig instance = new ApplicationConfig();
        Assert.assertEquals(null, instance.getOption("truc"));
        instance.setOption("truc", "bidulle");
        instance.setOption("machin", "chouette");
        instance.setOption("toto.truc", "AutreBidulle");
        instance.setOption("toto.specific", "theOne");
        Assert.assertEquals("bidulle", instance.getOption("truc"));

        ApplicationConfig sub = instance.getSubConfig("toto.");
        // test la surcharge du default
        Assert.assertEquals("AutreBidulle", sub.getOption("truc"));
        // test l'utilisation du default
        Assert.assertEquals("chouette", sub.getOption("machin"));
        // test une valeur specifique au sub
        Assert.assertEquals("theOne", sub.getOption("specific"));

        // TODO ajouter d'autres tests, pour les autres methodes surchargee
        // TODO ajouter d'autres tests pour des sub-(sub-(sub)) config
    }

    @Test
    public void getAsList() {
        List<String> asString = new ArrayList<>();
        asString.add(ApplicationConfig.class.getName());
        asString.add(ApplicationConfigTest.class.getName());

        List<Class> asClass = new ArrayList<>();
        asClass.add(ApplicationConfig.class);
        asClass.add(ApplicationConfigTest.class);

        ApplicationConfig instance = new ApplicationConfig();
        Assert.assertEquals(null, instance.getOption("truc"));
        Assert.assertEquals(Collections.<String>emptyList(), instance.getOptionAsList("truc").getOption());

        instance.setOption("truc", ApplicationConfig.class.getName()
                                   + "," + ApplicationConfigTest.class.getName());

        Assert.assertEquals(asString, instance.getOptionAsList("truc").getOption());
        Assert.assertEquals(asClass, instance.getOptionAsList("truc").getOptionAsClass());
    }

    @Test
    public void getMethods() {
        ApplicationConfig instance = new ApplicationConfig();
        Map<String, Method> result = instance.getMethods();
        Assert.assertTrue(result.containsKey("option"));
    }

    @Test
    public void getParams() throws Exception {
        Method m = DummyAction.class.getMethod("dummyAction", String.class, Integer.TYPE);
        List<String> list = new ArrayList<>(Arrays.asList("toto", "10", "/tmp", "9"));
        ListIterator<String> args = list.listIterator();

        ApplicationConfig instance = new ApplicationConfig();
        String[] expResult = new String[]{"toto", "10"};
        String[] result = instance.getParams(m, args);
        Assert.assertEquals(Arrays.asList(expResult), Arrays.asList(result));
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void testCreateAction() throws Exception {
        List<String> list = new ArrayList<>(Arrays.asList("dummy", "toto", "10", "/tmp", "9"));
        ListIterator<String> args = list.listIterator();
        args.next();
        ApplicationConfig instance = new ApplicationConfig();

        Action result = instance.createAction(
                DummyAction.class.getName() + "#dummyAction", args);
        Assert.assertEquals(1, result.step);
        DUMMY_ACTION_CALL = 0;
        result.doAction();
        Assert.assertEquals(1, DUMMY_ACTION_CALL);
    }

    @Test
    public void testParse() throws Exception {
        String[] args = "-f file -v -d -o /tmp/file -m coucou 10 others args".split(" ");
        ApplicationConfig instance = new ApplicationConfig();
        instance.addAlias("-f", "--option", "file");
        instance.addAlias("-v", "--option", "verbose", "true");
        instance.addAlias("-d", "--option", "debug", "true");
        instance.addAlias("-o", "--option", "output");
        instance.addAlias("-m", "--" + DummyAction.class.getName() + "#dummyAction");
        instance.parse(args);

        DUMMY_ACTION_CALL = 0;
        Assert.assertEquals("file", instance.getOption("file"));
        Assert.assertEquals("true", instance.getOption("verbose"));
        Assert.assertEquals("true", instance.getOption("debug"));
        Assert.assertEquals("/tmp/file", instance.getOption("output"));
        Assert.assertEquals(Arrays.asList("others", "args"), instance.getUnparsed());

        instance.doAction(1);
        Assert.assertEquals(1, DUMMY_ACTION_CALL);
    }

    /**
     * Test that system properties such as ${user.home}, ${user.name} are
     * replaced.
     *
     * @throws ArgumentsParserException if could not parse configuration
     */
    @Test
    public void testSystemProperties() throws ArgumentsParserException {
        ApplicationConfig instance = new ApplicationConfig();
        instance.parse();
        instance.printConfig();

        instance.setOption("hellomessage", "Hello ${user.name} !");

        Assert.assertEquals("Hello " + System.getProperty("user.name") + " !", instance.getOption("hellomessage"));
        Assert.assertEquals("Hello ${user.name} !", instance.getProperties(ApplicationConfigScope.OPTIONS).getProperty("hellomessage"));

        instance.setOption("tempdir", "${java.io.tmpdir}" + File.separator + "blah");
        File tempDir = instance.getOptionAsFile("tempdir");
        Assert.assertEquals(new File(System.getProperty("java.io.tmpdir"), "blah").getAbsolutePath(), tempDir.getAbsolutePath());

        instance.setOption("system", "${os.name}");
        instance.setOption("os", "${system}");
        instance.setOption("sysinfo", "I'm running ${os} :)");
        Assert.assertEquals("I'm running " + System.getProperty("os.name") + " :)", instance.getOption("sysinfo"));

        // test not found properties
        instance.setOption("notexists", "Attention ${blah.bloh.bluh} :(");
        Assert.assertEquals("Attention ${blah.bloh.bluh} :(", instance.getOption("notexists"));
    }

    /**
     * test if dot is replaced with _ if properties is not found with dot in env
     *
     * @throws ArgumentsParserException if could not parse configuration
     */
    @Test
    public void testEnvProperties() throws ArgumentsParserException {
        ApplicationConfig instance = new ApplicationConfig();
        // simulate env variable with _ to replace dot

        instance.getProperties(ApplicationConfigScope.ENV).put("test_env", "value");

        String value = instance.getOption("test.env");
        Assert.assertEquals("value", value);
    }

    @Test
    public void getUnparsed2() throws Exception {

        String[] args = "test --du i_am_a_test 2 --option file f1 --option verbose false -- --openui false --m coucou 10 others args".split(" ");
        ApplicationConfig instance = new ApplicationConfig();
        instance.addActionAlias("--du", DummyAction.class.getName() + "#" + "dummyAction");
        instance.parse(args);
        instance.doAction(1);

        log.info(instance.getUnparsed());
        Assert.assertEquals(8, instance.getUnparsed().size());
        Assert.assertEquals("test", instance.getUnparsed().get(0));
    }

    @Test
    public void getFlatOptions() throws Exception {

        ApplicationConfig instance = new ApplicationConfig();
        instance.parse();
        instance.setDefaultOption("user.firstname", "toto");
        instance.setDefaultOption("user.lastname", "tutu");
        instance.setOption("user.fullname", "${user.lastname} ${user.firstname}");

        Assert.assertEquals(1, instance.getOptions().size());
        // il y en a plus de 3 car il y a aussi les variables d'environnement
        Assert.assertTrue(instance.getFlatOptions().size() > 3);

        // test replacement and non replacement
        Assert.assertEquals("tutu toto",
                            instance.getFlatOptions().getProperty("user.fullname"));
        Assert.assertEquals("tutu toto",
                            instance.getFlatOptions(true).getProperty("user.fullname"));
        Assert.assertEquals("${user.lastname} ${user.firstname}",
                            instance.getFlatOptions(false).getProperty("user.fullname"));
    }

    @Test
    public void getNullOptions() throws Exception {
        ApplicationConfig instance = new ApplicationConfig();
        instance.parse();

        // primitives can not be null
        Assert.assertNotNull(instance.getOptionAsBoolean("dfsdfgqsgqfg"));
        Assert.assertNotNull(instance.getOptionAsDouble("dfsdfgqsgqfg"));
        Assert.assertNotNull(instance.getOptionAsInt("dfsdfgqsgqfg"));
        Assert.assertNotNull(instance.getOptionAsLong("dfsdfgqsgqfg"));
        // list option can not be null
        Assert.assertNotNull(instance.getOptionAsList("dfsdfgqsgqfg"));

        // all other types can be null
        Assert.assertNull(instance.getOptionAsClass("dfsdfgqsgqfg"));
        Assert.assertNull(instance.getOptionAsDate("dfsdfgqsgqfg"));
        Assert.assertNull(instance.getOptionAsFile("dfsdfgqsgqfg"));
        Assert.assertNull(instance.getOptionAsLocale("dfsdfgqsgqfg"));
        Assert.assertNull(instance.getOptionAsTime("dfsdfgqsgqfg"));
        Assert.assertNull(instance.getOptionAsTimestamp("dfsdfgqsgqfg"));
        Assert.assertNull(instance.getOptionAsURL("dfsdfgqsgqfg"));
        Assert.assertNull(instance.getOptionAsVersion("dfsdfgqsgqfg"));
    }

    @Test
    public void testPrintConfig() throws ArgumentsParserException, UnsupportedEncodingException {
        ApplicationConfig instance = new ApplicationConfig();
        instance.parse();
        instance.setOption("toto", "tata");

        // get content of printConfig
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (PrintStream ps = new PrintStream(baos)) {
            instance.printConfig(ps);
        }
        String content = baos.toString("UTF-8");

        if (log.isDebugEnabled()) {
            log.debug("printConfig = " + content);
        }

        Assert.assertTrue(content.indexOf("toto=tata") > 0);
    }

    @Test
    public void getOptionAsLocale() {
        Locale expected;
        Locale actual;

        ApplicationConfig instance = new ApplicationConfig();

        // test null locale
        actual = instance.getOptionAsLocale("toto");
        Assert.assertNull(actual);

        // test not null locale
        instance.setOption("toto", "fr");

        expected = Locale.FRENCH;
        actual = instance.getOptionAsLocale("toto");
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getOptionAsVersion() {
        Version expected;
        Version actual;

        ApplicationConfig instance = new ApplicationConfig();

        // test null version
        actual = instance.getOptionAsVersion("toto");
        Assert.assertNull(actual);

        // not null version
        instance.setOption("toto", "1.2");
        expected = VersionBuilder.create("1.2").build();
        actual = instance.getOptionAsVersion("toto");
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getOptionAsLong() {
        ApplicationConfig instance = new ApplicationConfig();

        // test long is not null
        long actual = instance.getOptionAsLong("toto");
        Assert.assertNotNull(actual);
        Assert.assertEquals(0L, actual);

        // not null version
        long expected = System.currentTimeMillis();
        instance.setOption("toto", "" + expected);
        actual = instance.getOptionAsLong("toto");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getOsName() throws Exception {
        ApplicationConfig config = new ApplicationConfig();
        config.parse();
        String v = config.getOsName();
        Assert.assertTrue(StringUtils.isNotBlank(v));
        if (log.isInfoEnabled()) {
            log.info("os.name: " + v);
        }
    }

    @Test
    public void getOsArch() throws Exception {
        ApplicationConfig config = new ApplicationConfig();
        config.parse();
        String v = config.getOsArch();
        Assert.assertTrue(StringUtils.isNotBlank(v));
        if (log.isInfoEnabled()) {
            log.info("os.arch: " + v);
        }
    }

    protected Properties loadPropertyFile(File file) throws IOException {
        FileInputStream inStream;

        inStream = FileUtils.openInputStream(file);
        try {
            Properties p = new Properties();
            p.load(inStream);
            inStream.close();
            return p;
        } finally {
            IOUtils.closeQuietly(inStream);
        }
    }

}
