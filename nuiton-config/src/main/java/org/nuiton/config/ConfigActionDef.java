package org.nuiton.config;

/*
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * Le contrat de marquage des actions, on utilise cette interface pour
 * caracteriser une action.
 *
 * Ex :
 *
 * <pre>
 * public enum MyAppConfigAction implements ConfigActionDef {
 *     HELP(MyAppHelpAction.class.getName() + "#show", "-h", "--help");
 *     public String action;
 *     public String[] aliases;
 *
 *     private WikittyConfigAction(String action, String... aliases) {
 *         this.action = action;
 *         this.aliases = aliases;
 *     }
 *
 *     &#64;Override
 *     public String getAction() {
 *         return action;
 *     }
 *
 *     &#64;Override
 *     public String[] getAliases() {
 *         return aliases;
 *     }
 *
 * }
 * </pre>
 *
 * @author sletellier
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.6.10
 */
public interface ConfigActionDef extends Serializable {

    /**
     * Must return fully qualified method path : package.Class#method
     *
     * @return action to run
     */
    String getAction();

    /**
     * Return all alias used to execute action.
     *
     * @return aliases used to execute action
     */
    String[] getAliases();

    /** @return la clef i18n de description de l'option */
    String getDescription();

}
