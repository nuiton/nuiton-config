package org.nuiton.config;

/*
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Properties;

/**
 * Permet de masquer un prefix. Il est possible d'avoir des valeurs par
 * defaut. Par exemple:
 * <pre>
 * monOption=toto
 * monPrefix.monOption=titi
 * </pre>
 * <ul>
 * <li>Si on cree le subApp avec le prefix "monPrefix." et qu'on demande la valeur
 * de "monOption", la valeur retournee est "titi".</li>
 * <li>Si on cree le subApp avec le prefix "monAutrePrefix." et qu'on demande la valeur
 * de "monOption", la valeur retournee est "toto" (valeur par defaut de monOption.</li>
 * </ul>
 * Certaines methodes retournees ne sont pas
 * surchargee et ne masque pas le prefix:
 * <ul><li>getOptions()</li></ul>
 *
 * @since 2.4.9
 */
public class SubApplicationConfig extends ApplicationConfig {

    protected ApplicationConfig parent;
    protected String prefix;

    public SubApplicationConfig(ApplicationConfig parent, String prefix) {
        this.parent = parent;
        this.prefix = prefix;
    }

    @Override
    protected void init(ApplicationConfigInit init) {
        // do nothing
    }

    public ApplicationConfig getParent() {
        return parent;
    }

    public String getPrefix() {
        return prefix;
    }

    @Override
    public Properties getOptions() {
        return getParent().getOptions();
    }

    @Override
    public void setDefaultOption(String key, String value) {
        getParent().setDefaultOption(getPrefix() + key, value);
    }

    @Override
    public boolean hasOption(String key) {
        return getOption(key) != null;
    }

    @Override
    public void setOption(String key, String value) {
        getParent().setOption(getPrefix() + key, value);
    }

    /**
     * Surcharge pour recherche la cle avec le prefix. Si on ne la retrouve
     * pas, on recherche sans le prefixe pour permettre d'avoir des valeurs
     * par défaut.
     *
     * @param key La cle de l'option
     * @return l'option trouvée avec le prefixe ou sinon celle sans le prefixe si pas trouvée.
     */
    @Override
    public String getOption(String key) {
        String result = getParent().getOption(getPrefix() + key);
        if (result == null) {
            result = getParent().getOption(key);
        }
        return result;
    }

    /**
     * Surcharge de la methode pour que les options commencant par le prefix
     * soit modifiee pour qu'elle est la meme cle sans le prefix. Le but
     * est de garder les autres options et si une option avait le meme nom
     * qu'elle soit effacee par celle dont on a supprime le prefix
     *
     * @param replaceInner le prefix à remplacer
     * @return les options commencant par le prefix
     * soit modifiee pour qu'elle est la meme cle sans le prefix. Le but
     * est de garder les autres options et si une option avait le meme nom
     * qu'elle soit effacee par celle dont on a supprime le prefix
     */
    @Override
    public Properties getFlatOptions(boolean replaceInner) {
        Properties result = getParent().getFlatOptions(replaceInner);
        Properties tmp = new Properties();
        int lenght = getPrefix().length();
        for (Map.Entry e : result.entrySet()) {
            String k = (String) e.getKey();
            if (k.startsWith(getPrefix())) {
                k = k.substring(lenght);
                String v = (String) e.getValue();
                tmp.setProperty(k, v);
            }
        }
        result.putAll(tmp);
        return result;
    }

    /**
     * Surcharge pour recupere les valeurs commencant par le prefix demande
     * en plus du prefix 'sub'. Les options sont ensuite fusionnee pour
     * permettre aussi les valeurs par defaut
     *
     * @param prefix prefix to use
     * @return les valeurs commençant par le prefix demandé en plus du prefix 'sub'.
     */
    @Override
    public Properties getOptionStartsWith(String prefix) {
        Properties result = getParent().getOptionStartsWith(prefix);
        Properties tmp = getParent().getOptionStartsWith(getPrefix() + prefix);
        int lenght = getPrefix().length();
        for (Map.Entry e : tmp.entrySet()) {
            String k = (String) e.getKey();
            k = k.substring(lenght);
            String v = (String) e.getValue();
            // on ajout/ecrase les valeurs de result
            result.setProperty(k, v);
        }
        return result;
    }

    @Deprecated
    @Override
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (propertyName.startsWith(getPrefix())) {
            propertyName = propertyName.substring(getPrefix().length());
            getParent().firePropertyChange(propertyName, oldValue, newValue);
        } // else not fire event
    }

    @Deprecated
    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        getParent().addPropertyChangeListener(getPrefix() + propertyName, listener);
    }

    @Deprecated
    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        getParent().removePropertyChangeListener(getPrefix() + propertyName, listener);
    }

    @Deprecated
    @Override
    public boolean hasListeners(String propertyName) {
        return getParent().hasListeners(getPrefix() + propertyName);
    }

    // methode interdite dans le sub
    @Override
    public ApplicationConfig parse(String... args) throws ArgumentsParserException {
        throw new UnsupportedOperationException("This method is not supported in SubApplicationConfig");
    }

}
