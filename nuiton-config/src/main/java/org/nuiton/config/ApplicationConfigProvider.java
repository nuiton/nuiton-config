package org.nuiton.config;

/*
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Locale;
import java.util.ServiceLoader;

/**
 * Provider of a {@link ApplicationConfig}.
 *
 * Each library of application which use {@link ApplicationConfig} should
 * implements this and add the provider available via the
 * {@link ServiceLoader} mecanism.
 *
 * Using such provider offers a nice way to find out what options can be loaded
 * in a application. It also offers a simply way to generate application
 * config report for documentation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4.8
 */
public interface ApplicationConfigProvider {

    /**
     * Returns the name of the provided application config.
     *
     * This should be the name of the library or application which offers
     * the configuration.
     *
     * @return the name of the provided application config
     */
    String getName();

    /**
     * Returns the localized description of the configuration.
     *
     * @param locale locale used to render description
     * @return the localized description of the configuration
     */
    String getDescription(Locale locale);

    /**
     * Returns all options offered by the configuration.
     *
     * @return all options offered by the configuration
     * @see ConfigOptionDef
     */
    ConfigOptionDef[] getOptions();

    /**
     * Returns all actions offered by the configuration.
     *
     * @return all actions offered by the configuration.
     * @see ConfigActionDef
     */
    ConfigActionDef[] getActions();
}
