package org.nuiton.config;

/*
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Helper about {@link ApplicationConfig}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4.8
 */
public class ApplicationConfigHelper {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ApplicationConfigHelper.class);

    protected ApplicationConfigHelper() {
        // helper with no instance
    }

    /**
     * Obtain all providers on class-path.
     *
     * @param classLoader optional classLoader used to seek for providers
     * @param includes    optional includes providers to use (if none then accept all providers)
     * @param excludes    optional excludes providers (if none the no reject)
     * @param verbose     verbose flag
     * @return sets of providers
     */
    public static Set<ApplicationConfigProvider> getProviders(ClassLoader classLoader,
                                                              Set<String> includes,
                                                              Set<String> excludes,
                                                              boolean verbose) {
        ServiceLoader<ApplicationConfigProvider> loader;
        if (classLoader == null) {
            loader = ServiceLoader.load(ApplicationConfigProvider.class);

        } else {
            loader = ServiceLoader.load(ApplicationConfigProvider.class,
                                        classLoader);
        }

        Set<ApplicationConfigProvider> result =
                new HashSet<>();

        for (ApplicationConfigProvider configProvider : loader) {
            String name = configProvider.getName();
            if (includes != null && !includes.contains(name)) {

                // reject by include
                if (verbose) {
                    log.info("configuration named '" + name +
                             "' is rejected by includes.");
                }
                continue;
            }
            if (excludes != null && excludes.contains(name)) {

                // reject by exclude
                if (verbose) {
                    log.info("configuration named '" + name +
                             "' is rejected by excludes.");
                }
                continue;
            }
            if (verbose) {
                log.info("configuration named '" + name +
                         "' will be generated.");
            }
            result.add(configProvider);
        }
        return result;
    }

    public static ApplicationConfigProvider getProvider(ClassLoader classLoader,
                                                        String name) {
        Set<ApplicationConfigProvider> providers = getProviders(
                classLoader, null, null, false);
        ApplicationConfigProvider result = null;
        for (ApplicationConfigProvider provider : providers) {
            if (name.equals(provider.getName())) {
                result = provider;
                break;
            }
        }
        return result;
    }

    /**
     * Load default options from all given config providers.
     *
     * @param config    config where to add default options.
     * @param providers providers to use
     * @since 2.6.7
     */
    public static void loadAllDefaultOption(ApplicationConfig config,
                                            Set<ApplicationConfigProvider> providers) {

        for (ApplicationConfigProvider provider : providers) {
            if (log.isInfoEnabled()) {
                log.info("Load default options from configuration: " +
                         provider.getName());
            }
            if (log.isInfoEnabled()) {
                for (ConfigOptionDef optionDef : provider.getOptions()) {
                    log.info(" " + optionDef.getKey() +
                             " (" + optionDef.getDefaultValue() + ')');
                }
            }
            config.loadDefaultOptions(provider.getOptions());
        }
    }

    /**
     * Load all actions from all given config providers.
     *
     * @param applicationConfig config where to add actions.
     * @param providers providers to use
     * @since 3.0
     */
    public static void loadAllActions(ApplicationConfig applicationConfig, Set<ApplicationConfigProvider> providers) {

        for (ApplicationConfigProvider provider: providers) {
            applicationConfig.loadActions(provider.getActions());
        }
    }


    /**
     * Gets all transient options from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options that are transient
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.7
     */
    public static Set<ConfigOptionDef> getTransientOptions(Set<ApplicationConfigProvider> providers) {
        Set<ConfigOptionDef> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isTransient()) {
                    result.add(def);
                }
            }
        }
        return result;
    }

    /**
     * Gets all final options from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options that are final
     * @see ConfigOptionDef#isFinal()
     * @since 2.6.7
     */
    public static Set<ConfigOptionDef> getFinalOptions(Set<ApplicationConfigProvider> providers) {
        Set<ConfigOptionDef> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isFinal()) {
                    result.add(def);
                }
            }
        }
        return result;
    }

    /**
     * Get all option keys that should not be saved in the user config file
     * from the given options providers.
     *
     * Such options are {@code transient} or {@code final}.
     *
     * @param providers providers to inspect
     * @return the set of options key not to store in the config file
     * @see ConfigOptionDef#isFinal()
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.11
     */
    public static Set<String> getTransientOrFinalOptionKey(Set<ApplicationConfigProvider> providers) {
        Set<String> result = new HashSet<>();
        result.addAll(getTransientOptionKeys(providers));
        result.addAll(getFinalOptionKeys(providers));
        return result;
    }

    /**
     * Gets all transient options keys from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options key that are transient
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.11
     */
    public static Set<String> getTransientOptionKeys(Set<ApplicationConfigProvider> providers) {
        Set<String> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isTransient()) {
                    result.add(def.getKey());
                }
            }
        }
        return result;
    }

    /**
     * Gets all final options keys from the given providers.
     *
     * @param providers providers to inspect
     * @return the set of all options keys that are final
     * @see ConfigOptionDef#isTransient()
     * @since 2.6.7
     */
    public static Set<String> getFinalOptionKeys(Set<ApplicationConfigProvider> providers) {
        Set<String> result = new HashSet<>();
        for (ApplicationConfigProvider provider : providers) {
            for (ConfigOptionDef def : provider.getOptions()) {
                if (def.isFinal()) {
                    result.add(def.getKey());
                }
            }
        }
        return result;
    }
}
