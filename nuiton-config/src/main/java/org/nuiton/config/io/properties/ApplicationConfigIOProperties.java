package org.nuiton.config.io.properties;

/*-
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.io.ApplicationConfigIO;
import org.nuiton.config.io.ApplicationConfigReadFormat;
import org.nuiton.config.io.ApplicationConfigReadPropertiesException;
import org.nuiton.config.io.ApplicationConfigWritePropertiesException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Properties;

/**
 * Created on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
public class ApplicationConfigIOProperties implements ApplicationConfigIO {

    @Override
    public boolean accept(ApplicationConfigReadFormat readFormat) {
        return ApplicationConfigReadFormat.properties.equals(readFormat);
    }

    @Override
    public Properties readProperties(URL url, String encoding) throws ApplicationConfigReadPropertiesException {
        Properties properties = new Properties();
        try (Reader reader = new BufferedReader(new InputStreamReader(url.openStream(), encoding))) {
            properties.load(reader);
        } catch (Exception e) {
            throw new ApplicationConfigReadPropertiesException("Can't read properties file: " + url, e);
        }
        return properties;
    }

    @Override
    public void writeProperties(Properties properties, File file, String encoding, String comment) throws ApplicationConfigWritePropertiesException {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoding))) {
            properties.store(writer, comment);
        } catch (Exception e) {
            throw new ApplicationConfigWritePropertiesException("Can't store properties file: " + file, e);
        }
    }

}
