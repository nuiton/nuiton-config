package org.nuiton.config.io;

/*-
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.net.URL;
import java.util.Properties;
import java.util.ServiceLoader;

/**
 * Created on 07/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.1
 */
public class ApplicationConfigIOHelper implements ApplicationConfigIO {

    private final ApplicationConfigIO delegate;

    public ApplicationConfigIOHelper(ApplicationConfigReadFormat readFormat) {
        if (readFormat == null) {
            throw new NullPointerException("Can't init with null readFormat");
        }
        ServiceLoader<ApplicationConfigIO> loader = ServiceLoader.load(ApplicationConfigIO.class);
        ApplicationConfigIO delegate = null;
        for (ApplicationConfigIO applicationConfigIO : loader) {
            if (applicationConfigIO.accept(readFormat)) {
                delegate = applicationConfigIO;
            }
        }
        if (delegate == null) {
            throw new IllegalStateException("Could not find ApplicationConfigIO implementation for format: " + readFormat);
        }
        this.delegate = delegate;
    }

    @Override
    public boolean accept(ApplicationConfigReadFormat readFormat) {
        // accept all formats
        return true;
    }

    @Override
    public Properties readProperties(URL url, String encoding) throws ApplicationConfigReadPropertiesException {
        return delegate.readProperties(url, encoding);
    }

    @Override
    public void writeProperties(Properties properties, File file, String encoding, String comment) throws ApplicationConfigWritePropertiesException {
        delegate.writeProperties(properties, file, encoding, comment);
    }

}
